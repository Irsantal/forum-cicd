# forum-project

Slides : https://docs.google.com/presentation/d/19pTOFLmaZsRP9Zrq-vGcyuEzqzMpOS1e2g_1Se1CVFo/edit?usp=sharing


#deploy prod : docker-compose -f docker-compose.prod.yaml up


# quelle est ma stratégie de CI et de déploiement ?

# utiliser gitlab CI pour créer des images avec mon code mis à jour pour le front et le back
# les enregistrer dans un registry si les tests passent
# jouer sur un server distant uniquement un docker compose des images mises à jour

# j'ai pensé la CI en deux étapes 
# 1 construire les images + jouer les tests + mettre à jour le repo
# 2 jouer un déploiement manuel avec click sur un bouton